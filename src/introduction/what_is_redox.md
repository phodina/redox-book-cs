Co je Redox?
============

Možná máte stále otázku: Co je vlastně Redox?

Redox je pokus stvořit úplný plně funkční operační systém s ohledem na bezpečnost, svobodu, spolehlivost, správnost a pragmatizmus.

Cíle Redoxu
-----------

Chceme, aby jej bylo možné používat bez omezení jako alternativu k Linuxu na vašich počítačích. Dále by měl být schopen spustit většinu Linuxových programů s minimálními modifikacemi (see [Why Free Software]).

Cílem je mít vše v Rustu. Toto je volba v návrhu, která doufejme zlepší správnost a bezpečnost (see [Why Rust]).

Bezpečnost návrhu zlepšíme v porovnání s ostatními Unixovými systémy použitím bezpečným výchozím nastvením a zakázáním nebezpečných konfigurací, kde jen to bude možné.

The non-goals of Redox
----------------------

We are not a Linux clone, or POSIX-compliant, nor are we crazy scientists, who wish to redesign everything. Generally, we stick to well-tested and proven correct designs. If it ain't broken don't fix it.

This means that a large number of standard programs and libraries will be compatible with Redox. Some things that do not align with our design decisions will have to be ported.

The key here is the trade off between correctness and compatibility. Ideally, you should be able achieve both, but unfortunately, you can't always do so.

[Why Free Software]: ./introduction/why_free_software.html
[Why Rust]: ./introduction/why_rust.html
