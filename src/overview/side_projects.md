Postranní projekty
==================

Redox je úplný operační systém.
Kromě jádra vyvíjíme i řadu postranních projektů, včetně:

- [TFS]: Souborový systém inspirovaný ZFS.
- [Ion]: Redox shell.
- [Orbital]: Displej server pro Redox.
- [OrbTK]:  Widget toolkit.
- [pkgutils]: Knihovna pro správa balíčků na Redoxu a frontend pro konzoli.
- [Sodium]: Editor podobný Vi.
- [ralloc]: Alokátor paměti.
- [libextra]: Doplněk pro knihovnu libstd, používaný napříč kódem Redox.
- [games-for-redox]: Kolekce malých her pro Redox (podobné BSD hrám).
- a pár dalších vzrušujících projektů, které [zde] můžete prozkoumat.

Máme též 3 distribuce nástrojů, které jsou kolekcí malých užitečných konzolových programů:

- [Coreutils]: Minimální množina nástrojů pro použitelný systém.
- [Extrautils]: Dodatečné nástroje typu upomínek, kalendářů, kontroly pravopisu a dalších.
- [Binutils]: Nástroje pro práci s binárními soubory.

Přispíváme také do projektů třetích stran, které jsou velmi používané v Redoxu.

 - [uutils/coreutils]: Multiplatformní přepis v Rustu GNU coreutils.
 - [m-labs/smoltcp]: Síťový zásobník používaný Redoxem.

Jaké nástroje jsou vhodné pro distribuci Redoxu?
-------------------------------------------------

Některé z těchto nástrojů se vyčlení z výchozí distribuce do volitelných balíčků. Příkladem je Orbital, OrbTK, Sodium a další.

Nástroje ze seznamu spadají do jedné z následujících kategorií:

1. **Kritické**, ty které jsou nutné pro plně funkční a použitelný systém.
2. **Přátelské k ekosystému**, ty které jso zde z důvodu konzistence.
3. **Zábavné**, ty které je příjemné mít a jsou úžasně jednoduché.

První kategorie by měla být zřejmá: OS bez určitých nástrojů je nepoužitelný OS.. Druhá kategorie obsahuje nástroje, které pravděpodobně nebudou výchozí v budoucnosti, nicméně teď jsou součástí oficiální distribuce. Třetí kategorie je zde pro pohodlí: zejména pro konzistenci a integritu infrastruktury Redoxu (např. pkgutils, OrbTK, and libextra).

Je důležité zmínit, že se pokoučíme vyhnout nástrojům nepsaným v Rustu pro bezpečnost a konzistenci (více v [Why Rust]).

[TFS]: https://gitlab.redox-os.org/redox-os/tfs
[Ion]: https://gitlab.redox-os.org/redox-os/ion
[Orbital]: https://gitlab.redox-os.org/redox-os/orbital
[OrbTK]: https://gitlab.redox-os.org/redox-os/orbtk
[pkgutils]: https://gitlab.redox-os.org/redox-os/pkgutils
[Sodium]: https://gitlab.redox-os.org/redox-os/sodium
[ralloc]: https://gitlab.redox-os.org/redox-os/ralloc
[libextra]: https://gitlab.redox-os.org/redox-os/libextra
[games-for-redox]: https://gitlab.redox-os.org/redox-os/games
[here]: https://gitlab.redox-os.org/redox-os

[Coreutils]: https://gitlab.redox-os.org/redox-os/coreutils
[Extrautils]: https://gitlab.redox-os.org/redox-os/extrautils
[Binutils]: https://gitlab.redox-os.org/redox-os/binutils

[uutils/coreutils]: https://gitlab.redox-os.org/uutils/coreutils
[m-labs/smoltcp]: https://gitlab.redox-os.org/m-labs/smoltcp

[Why Rust]: ./introduction/why_rust.html
