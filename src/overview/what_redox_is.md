Co je Redox
=============

Redox je operační systém spolu, který je spolu s integrovanými komponentami napsán čistě v jazyce [Rust]. Naším cílem je poskytnout plně funkční mikrojádro podobné Unixu, které je bezpečné a zadarmo.

Dodržujeme skromnou kompatibilitu s rozhraním [POSIX], čímž umožňujeme běh mnoha programů na Redoxu bez nutnosti portování.

Bereme inspiraci z [Plan9], [Minix], [Linux], and [BSD]. Pokoušíme se zobecnit různé koncepty z ostatních systémů, abych dostali jeden souhrný návrh. Více se dozvíte v kapitole [Návrh] chapter.

Momentálně Redox podporuje:

* Všechny x86-64 CPU.
* Grafické karty s podporou VBE (všechny Nvidia, Intel, and AMD karty z minulé dekády).
* AHCI disky.
* E1000 or RTL8168 síťové karty.
* Intel HDA audio řadič.
* Myš a klávesnice skrze PS/2 emulaci.

Tato kniha je rozdělena do 9 částí:

- [Overview]: Stručný přehled Redoxu.
- [Introduction]: Vysvětlení co Redox je a porovnání s ostatními systémy.
- [Getting started]: Kompilace a spuštění Redoxu.
- [The design]: Detailní popis návrhu a implementace Redoxu.
- Development in user space: Psaní aplikací pro Redox.
- [Contributing]: Jak můžete přispět do Redoxu.
- Understanding the code base: Ztotožnění se s kódem.
- Fun: Tajná kapitola.
- The future: Kam Redox míří.

Kaptioly jsou psány tak, že nepotřebujete znát Rust či vývoj operačních systémů.

[Rust]:  https://www.rust-lang.org
[POSIX]: https://en.wikipedia.org/wiki/POSIX
[Plan9]: http://9p.io/plan9/index.html
[Minix]: http://www.minix3.org/
[Linux]: https://en.wikipedia.org/wiki/Linux
[BSD]: http://www.bsd.org/

[Design]: ./design/design.html
[Overview]: ./overview/welcome.html
[Introduction]: ./introduction/why_redox.html
[Getting started]: ./getting_started/preparing_the_build.html
[The design]: ./design/design.html
[Contributing]: ./contributing/chat.html
