Vítejte!
========

Toto je kniha popisuje (téměř) vše v Redoxu: návrh, filozofii, jak funguje, jak můžete přispět, jak systém nasadit a mnoho dalšího.

Berte prosím na vědomí, že na knize se stíle ještě pracuje.

Tuto knihu napsal Ticki s pomocí LazyOxen, Steve Klabnik, ElijahCaine, and Jackpot51.

Pokud chcete ihned vyzkoušet Redox jděte na [začínáme](getting_started/getting_started.html).
